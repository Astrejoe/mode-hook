import {createContext, useState} from "react";
import {PaletteMode} from "@mui/material";

type modeContext = {
    mode: PaletteMode,
    setMode: (mode: PaletteMode) => void,
    toggleMode: () => void
}

function useMode(initialMode: PaletteMode = "light"): modeContext {
    const [mode, setMode] = useState<PaletteMode>(initialMode)
    const toggleMode = () => {
        setMode(prevMode => prevMode === "light" ? "dark" : "light")
    }

    return {
        mode,
        setMode,
        toggleMode
    }
}

const defaultModeContext: modeContext = {
    mode: "light",
    setMode: (mode: PaletteMode) => {},
    toggleMode: () => {}
}


const ModeContext = createContext<modeContext>(defaultModeContext)

export type { modeContext }
export { useMode, ModeContext }
